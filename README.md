# Laravel Backend Api

This is a step-by-step guide to setup Laravel Backend Api

## Install composer

```bash
composer install
```

## Duplicate .env file and update database setting

```bash
cp .env.example .env
```

## Generate key

```bash
php artisan key:generate
```

## Run migration and seeder

```bash
php artisan migrate --seed
```

## Serve your application

```bash
php artisan serve
```

