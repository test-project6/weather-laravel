<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            'London',
            'Berlin',
            'Liverpool',
            'Chicago',
            'Boston',
            'Austin',
            'Portland',
            'Cornwall'
        ];

        foreach($cities as $name)
        {
            DB::table('cities')->insert(
                [
                    'name' => $name,
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            );
        }
    }
}
