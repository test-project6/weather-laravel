<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('city_id')->index();
            $table->string('city')->index();
            $table->smallInteger('temperature');
            $table->smallInteger('min_temperature');
            $table->smallInteger('max_temperature');
            $table->string('weather');
            $table->string('weather_description');
            $table->longText('weather_icon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_infos');
    }
}
