<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(["namespace" => "Api"], function() {
    Route::group(["prefix" => "city"], function() {
        Route::post('/create/{city}', 'WeatherController@create');
        Route::post('/sync', 'WeatherController@sync');
        Route::get('/', 'WeatherController@getAllCity');
        Route::delete('/delete/{id}', 'WeatherController@delete');

        Route::get('/list', 'CityController@getCityList');
        Route::post('/create-list/{name}', 'CityController@create');
    });
});


