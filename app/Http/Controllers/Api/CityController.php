<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    public function getCityList()
    {
        $city = City::all(['name']);
        $list = [];

        if(count($city) > 0)
        {
            foreach($city as $row)
            {
                $list[] = $row->name;
            }
        }

        return response()->json($list, 200);
    }

    public function create($name)
    {
        $exist = City::where('name', $name)->first();

        if(!$exist)
        {
            try {
                DB::beginTransaction();
                $city = new City();
                $city->name = ucfirst($name);
                $city->save();

                DB::commit();
            } catch (\PDOException $e) {
                DB::rollBack();

                return response()->json([
                    "message" => $e->getMessage()
                ], 400);
            }
        }

        return redirect('/api/city/list');
    }
}
