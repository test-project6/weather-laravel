<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\WeatherInfo;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WeatherController extends Controller
{
    private function apiKey()
    {
        return $key = '05b6adb251dad23bfd6a67a0ee9ba918';
    }

    private function createUpdate($data, $action)
    {
        if($action == 'create')
        {
            $weather = new WeatherInfo();
            $weather->city_id = $data['city_id'];
        }
        else
        {
            $weather = WeatherInfo::where('city_id', $data['city_id'])->first();
        }

        $weather->city = $data['city'];
        $weather->temperature = $data['temperature'];
        $weather->min_temperature = $data['min_temperature'];
        $weather->max_temperature = $data['max_temperature'];
        $weather->weather = $data['weather'];
        $weather->weather_description = $data['weather_description'];
        $weather->weather_icon = "http://openweathermap.org/img/w/" .$data['weather_icon']. ".png";
        $weather->save();

        return $weather;
    }

    public function getAllCity(Request $request)
    {
        $result = WeatherInfo::all();
        return response()->json($result, 200);
    }

    public function create(Request $request, $city)
    {
        $client = new Client();

        $res = $client->request('POST',
            'api.openweathermap.org/data/2.5/weather?q='.$city.'&appid='.$this->apiKey().'&units=metric');

        $result = json_decode($res->getBody(), true);

        if(isset($result['id']))
        {
            $exist = WeatherInfo::where('city_id', $result['id'])->first();
            $data['city_id'] = $result['id'];
            $data['city'] = $result['name'];
            $data['temperature'] = round($result['main']['temp']);
            $data['min_temperature'] = round($result['main']['temp_min']);
            $data['max_temperature'] = round($result['main']['temp_max']);
            $data['weather'] = $result['weather'][0]['main'];
            $data['weather_description'] = ucfirst($result['weather'][0]['description']);
            $data['weather_icon'] = $result['weather'][0]['icon'];

            try {
                DB::beginTransaction();

                if($exist)
                    $this->createUpdate($data, 'update');
                else
                    $this->createUpdate($data, 'create');

                DB::commit();
            } catch (\PDOException $e) {
                DB::rollBack();

                return response()->json([
                    "message" => $e->getMessage()
                ], 400);
            }
        }
        else
        {
            return response()->json([
                "message" => $result
            ], 400);
        }

        $result = WeatherInfo::all();
        return response()->json($result, 200);
    }

    public function sync(Request $request)
    {
        $weather = WeatherInfo::all(['city_id']);

        if(count($weather) > 0)
        {
            foreach($weather as $row)
            {
                $weatherArray[] = $row->city_id;
            }

            $city_id = implode(",", $weatherArray);
            $client = new Client();

            $res = $client->request('POST',
                'api.openweathermap.org/data/2.5/group?id=' . $city_id . '&appid=' . $this->apiKey() . '&units=metric');

            $result = json_decode($res->getBody(), true);

            if (isset($result['list']))
            {
                foreach ($result['list'] as $row)
                {
                    $data['city_id'] = $row['id'];
                    $data['city'] = $row['name'];
                    $data['temperature'] = round($row['main']['temp']);
                    $data['min_temperature'] = round($row['main']['temp_min']);
                    $data['max_temperature'] = round($row['main']['temp_max']);
                    $data['weather'] = $row['weather'][0]['main'];
                    $data['weather_description'] = ucfirst($row['weather'][0]['description']);
                    $data['weather_icon'] = $row['weather'][0]['icon'];

                    try {
                        DB::beginTransaction();

                        $this->createUpdate($data, 'update');

                        DB::commit();
                    } catch (\PDOException $e) {
                        DB::rollBack();

                        return response()->json([
                            "message" => $e->getMessage()
                        ], 400);
                    }
                }
            } else {
                return response()->json([
                    "message" => $result
                ], 400);
            }

            $result = WeatherInfo::all();
            return response()->json($result, 200);
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            WeatherInfo::find($id)->delete();

            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();

            return response()->json([
                "message" => $e->getMessage()
            ], 400);
        }

        $result = WeatherInfo::all();
        return response()->json($result, 200);
    }

}
